import React, { useEffect, useState } from "react";
import axiosBlog from "../../axios-blog";
import moment from "moment";
import "./PostEdit.css";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

const PostEdit = (props) => {
  const [post, setPost] = useState({
    title: "",
    description: "",
  });
  const id = props.match.params.id;

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosBlog.get("/posts/" + id + ".json");

      setPost({ ...response.data.post });
    };
    fetchData().catch(console.error);
  }, [id]);

  const postaDataChanged = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    setPost((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const postHandler = async (event) => {
    event.preventDefault();

    const posts = {
      post: { ...post, date: new Date() },
    };

    try {
      await axiosBlog.put("/posts/" + id + ".json", posts);
    } finally {
      props.history.push("/");
    }
  };

  return (
    <>
      <form className="PostEdit" onSubmit={postHandler}>
        <h3>Edit Post</h3>
        <label>
          Title
          <input
            type="text"
            name="title"
            className="Field"
            value={post.title}
            onChange={postaDataChanged}
          />
        </label>
        <label>
          Description
          <textarea
            type="text"
            name="description"
            cols="100"
            rows="10"
            className="Field"
            value={post.description}
            onChange={postaDataChanged}
          />
        </label>
        <p>Время последнего изменения {moment(post.date).format("L LTS")}</p>
        <button>Save</button>
      </form>
    </>
  );
};

export default withErrorHandler(PostEdit, axiosBlog);
