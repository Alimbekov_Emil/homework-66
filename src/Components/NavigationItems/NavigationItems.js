import React from "react";
import NavigationItem from "./NavigationItem/NavigationItem";
import "./NavigationItems.css";

const NavigationItems = () => {
  return (
    <ul className="NavigationItems">
      <NavigationItem to="/" exact>
        Home
      </NavigationItem>
      <NavigationItem to="/posts/add" exact>
        Add
      </NavigationItem>
    </ul>
  );
};

export default NavigationItems;
