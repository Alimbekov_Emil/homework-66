import React from "react";
import NavigationItems from "../NavigationItems/NavigationItems";
import "./Layout.css";

const Layout = (props) => {
  return (
    <>
      <header className="header">
        <nav>
          <NavigationItems />
        </nav>
      </header>
      <main className="Layout-Content">{props.children}</main>
    </>
  );
};

export default Layout;
