import { Route, Switch } from "react-router-dom";
import "./App.css";
import Layout from "./Components/Layout/Layout";
import PostEdit from "./Components/PostEdit/PostEdit";
import ReadMore from "./Components/ReadMore/ReadMore";
import AddPage from "./Container/AddPage/AddPage";
import HomePage from "./Container/HomePage/HomePage";

const App = () => (
  <Layout>
    <div className="container">
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/posts/add" component={AddPage} />
        <Route path="/posts/:id" component={ReadMore} />
        <Route path="/edit/:id" component={PostEdit} />
        <Route render={() => <h1>Not found</h1>} />
      </Switch>
    </div>
  </Layout>
);

export default App;
