import React, { useEffect, useMemo, useState } from "react";
import Modal from "../../Components/UI/Modal/Modal";

const withErrorHandler = (WrappedComponent, axios) => {
  return function WithErrorHandler(props) {
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const reqId = useMemo(() => {
      return axios.interceptors.request.use(
        (res) => {
          setLoading(true);
          return res;
        },
        (error) => {
          setLoading(false);
          setError(error);
          throw error;
        }
      );
    }, []);

    const resId = useMemo(() => {
      return axios.interceptors.response.use(
        (res) => {
          setLoading(false);
          return res;
        },
        (error) => {
          setLoading(false);
          setError(error);
          throw error;
        }
      );
    }, []);

    useEffect(() => {
      return () => {
        axios.interceptors.response.eject(resId);
        axios.interceptors.request.eject(reqId);
      };
    }, [reqId, resId]);

    return (
      <>
        <Modal show={!!loading} />
        <WrappedComponent {...props} />
      </>
    );
  };
};

export default withErrorHandler;
